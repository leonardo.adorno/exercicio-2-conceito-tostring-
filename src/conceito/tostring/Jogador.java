/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conceito.tostring;

/**
 *
 * @author Aluno
 */
public class Jogador {
    
    private int vida;
    private int mana;
    private String nome;
    private Magia[] magias;
    private int qtdMagia;
    private int qtdItem;
    
    
    public Jogador(){
        vida=10;
        mana=10;
        nome="";
        magias=new Magia[3];
        qtdMagia=0;
        qtdItem=0;
        
        System.out.println("configurando o jogador...");
    }
    
    public void exMagia(int i){
        System.out.println("Lançando magia" + magias[i]);
        mana=mana-magias[i].getCusto();
        System.out.println("Estou com " + mana + " de mana");
}
    
    public void addMagia(Magia magia) {
        magias[qtdMagia]= magia;
        qtdMagia++;
    }
    
    public String toString(){
        return "Nome: " + getNome() + " Vida: " + getVida() + " Mana: "+ getMana();
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public String getNome() {
        
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Magia[] getMagias() {
        return magias;
    }

    public void setMagias(Magia[] magias) {
        this.magias = magias;
    }


    public void mostrarInventario(Inventario item){
        int i;
        
        for(i=0;i<item.listaDeItens.length;i++){
            if(item.listaDeItens[i] != null ){
            System.out.println("Inventario: " + item.listaDeItens[i]);
            }
        }
    }
}



